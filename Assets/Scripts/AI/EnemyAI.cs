﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Opsive.UltimateCharacterController.Traits;

namespace Project.Corridor.AI
{
    public class EnemyAI : MonoBehaviour
    {
        #region Editor Fields
        [SerializeField] protected float _attackTime;
        [SerializeField] protected float _attackRange;
        [SerializeField] protected Transform _target;
        [SerializeField] protected float _destroyAtY;
        [SerializeField] protected int _damage;
        [SerializeField] protected float _damageDelay;
        [SerializeField] protected DamageBox[] _damageBoxes;
        #endregion

        #region Inner Fields
        protected bool _attacking = false;
        protected bool _canDamage = true;
        protected Animator _anim;
        protected LookAt _lookAt;
        protected NavMeshAgent _agent;
        protected Transform _agentTransform;
        #endregion

        #region Unity Messages
        // Use this for initialization
        void Start()
        {
            FindTarget();
            _agent = GetComponentInChildren<NavMeshAgent>();
            _anim = GetComponentInChildren<Animator>();
            _lookAt = GetComponentInChildren<LookAt>();
            _agentTransform = _agent.transform;
            //_agent.updatePosition = false;

            if(_damageBoxes != null && _damageBoxes.Length > 0)
            {
                foreach(var box in _damageBoxes)
                {
                    box.DamageEvent.AddListener(DoDamage);
                }
            }else
            {
                Debug.Log("Enemy can't damage withot damage boxes");
            }
        }

        // Update is called once per frame
        void Update()
        {

            if (_agentTransform.position.y < _destroyAtY)
            {
                Destroy(this.gameObject);
                return;
            }

            _agent.destination = _target.position;

            var attack = CanAttack();

            Vector3 worldDeltaPosition = _agent.nextPosition - _agentTransform.position;

            if (worldDeltaPosition.magnitude > _agent.radius)
                _agent.nextPosition = _agentTransform.position;


            if (_agent.destination != null && !attack)
            {
                _anim.SetFloat("Velocity", _agent.velocity.magnitude);
            }
            else
            {
                _anim.SetFloat("Velocity", 0f);
            }

            if(_agent.destination != null && !_attacking && attack)
            {
                _attacking = true;
                _agent.isStopped = true;
                _anim.SetTrigger("Attack");
                StartCoroutine(FinishAttack());
            }

            if(_lookAt != null)
            {
                _lookAt.lookAtTargetPosition = _agent.steeringTarget + _lookAt.transform.forward;
            }

            //gameObject.transform.position = _agentTransform.position;

        }

        void OnAnimatorMove()
        {
            // Update position based on animation movement using navigation surface height
            Vector3 position = _anim.rootPosition;
            position.y = _agent.nextPosition.y;
            _agentTransform.position = position;
        }
        #endregion

        protected void FindTarget()
        {
            if(_target != null)
            {
                return;
            }

            _target = GameObject.FindGameObjectWithTag("Player")
                .GetComponent<Transform>();
            if (_target == null)
            {
                Debug.LogError("AI can't find target");
            }
        }

        protected bool CanAttack()
        {
            if(_target == null)
            {
                return false;
            }
            
            if (Vector3.Distance(_agentTransform.position, _target.position) <= _attackRange)
            {
                return true;
            }

            return false;

        }

        #region Coroutines
        protected IEnumerator FinishAttack()
        {
            yield return new WaitForSeconds(_attackTime);
            _attacking = false;
            _agent.isStopped = false;
        }

        /// <summary>
        /// Allow to do damage after delay
        /// </summary>
        protected IEnumerator AllowDamage()
        {
            yield return new WaitForSeconds(_damageDelay);
            _canDamage = true;
        }
        #endregion

        #region Event Handlers
        protected void DoDamage(Collider c)
        {
            if(!_canDamage)
            {
                return;
            }

            var ch = c.gameObject.GetComponentInParent<CharacterHealth>();

            if(ch == null)
            {
                return;
            }

            ch.Damage(_damage);

            if(_damageDelay > Mathf.Epsilon)
            {
                _canDamage = false;
                StartCoroutine(AllowDamage());
            }
        }
        #endregion
    }
}
