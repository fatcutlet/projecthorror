﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.Dynamics;
using UnityEngine.AI;
using Opsive.UltimateCharacterController.Traits;
using Opsive.UltimateCharacterController.Events;

namespace Project.Corridor.AI
{

    public class EnemyAI2 : MonoBehaviour
    {
        private const string CAMERA_TAG = "MainCamera";
        private const string PLAYER_TAG = "Player";
        private const string ATACK_TRIGGER = "Attack";
        private const string ATTACK_TYPE = "AttackType";

        // Start is called before the first frame update
        #region Editor Fields  
        [SerializeField] protected float _wakeUpDistance = 5;
        [SerializeField] protected int[] _obstaclesLayers = new int[] { 10, 25, 31 };
        [SerializeField] protected int _enemyLayer = 11;
        [SerializeField] protected Transform _root; //pelvis if puppetmaster
        [SerializeField] protected float _slowDownDistance = 5f;
        [SerializeField] protected float _slowDownSpeed = .5f;
        [SerializeField] protected float _attackDistance = .2f;
        [SerializeField] protected float _attackSpeed = 1.6f;
        [SerializeField] protected float _destroyAtY = -2f;
         #endregion

        #region Inner Fields
        protected PuppetMaster _puppet;
        protected bool _alive;
        protected bool _follow = false;
        protected bool _attacking = false;
        protected GameObject _player;
        protected LookAt _lookAt;
        protected NavMeshAgent _agent;
        protected Transform _agentTransform;
        protected Transform _target;
        protected Animator _anim;
        protected Health _health;
        protected System.Random _random = new System.Random();
        protected float _fullSpeed;
        protected float _distance;
        protected Rigidbody _rigidbody;
        

        protected readonly float _wakeUpDelay = 4f;
        #endregion

        #region Properties
        public Vector3 Position { get { return _root.position; } }
        #endregion

        #region Unity Messages
        void Start()
        {
            _puppet = GetComponentInChildren<PuppetMaster>();

            if (_puppet == null)
            {
                Debug.LogError("Can't find Puppet Master component in children");
            }

            //enemy should sleep
            _puppet.state = PuppetMaster.State.Frozen;
            _alive = true;
            _lookAt = GetComponentInChildren<LookAt>();
            _health = GetComponent<Health>();

            if (_health == null)
            {
                Debug.LogError("Can't fing Health component");
            }

            _agent = GetComponentInChildren<NavMeshAgent>();
            if (_agent == null)
            {
                Debug.LogError("Can't fing NavMeshAgent in children");
            }

            _agent.updatePosition = false;
            _agentTransform = _agent.transform;

            _player = GameObject.FindGameObjectWithTag(CAMERA_TAG);
            _target = GameObject.FindGameObjectWithTag(PLAYER_TAG).transform;
            _anim = GetComponentInChildren<Animator>();
            _rigidbody = _agent.gameObject.GetComponent<Rigidbody>();

            if(_player == null)
            {
                Debug.LogError("Can't find game the object witch tag " + CAMERA_TAG);
            }

            if(_target == null)
            {
                Debug.LogError("Can't find game the object witch tag " + PLAYER_TAG);
            }

            if(_anim == null)
            {
                Debug.LogError("Can't fing Animator in children");
            }

            if (_rigidbody == null)
            {
                Debug.LogError("Can't find RigidBody component in children");
            }

            _fullSpeed = _agent.speed;

            EventHandler.RegisterEvent<Vector3, Vector3, GameObject>(gameObject, "OnDeath", OnDeath);

        }

        // Update is called once per frame
        void Update()
        {

            if(_agentTransform.position.y < _destroyAtY)
            {
                Destroy(gameObject);
            }

            _distance = Vector3.Distance(_player.transform.position,  _agentTransform.position);

            if (_alive && _puppet.state == PuppetMaster.State.Frozen)
            {
                CheckWakeUp();
            }

            if(_alive && _follow && _puppet.state == PuppetMaster.State.Alive)
            {
                Follow();
                Attack();
            }

            

        }
        #endregion

        #region Protected Methods
        protected void CheckWakeUp()
        {
            

            if (_distance > _wakeUpDistance)
            {
                return;
            }

            var position = new Vector3(_root.position.x, _root.position.y, _root.position.z);

            var mask = 1 << _enemyLayer;

            foreach (int layer in _obstaclesLayers)
            {
                mask = mask | 1 << layer;
            }

            var x = position.x - _player.transform.position.x;
            var y = position.y - _player.transform.position.y;
            var z = position.z - _player.transform.position.z;
            var direction = new Vector3(x, y, z);

            var forward = _player.transform.TransformDirection(Vector3.forward);

            var angle = Vector3.Angle(forward, direction);

            if (angle > 60)
            {
                return;
            }

            RaycastHit hit;
            if (!Physics.Raycast(_player.transform.position, direction, out hit, _wakeUpDistance, mask))
            {
                return;
            }

            if (hit.collider.gameObject.layer != _enemyLayer)
            {
                return;
            }

            Debug.DrawRay(_player.transform.position, direction * hit.distance, Color.yellow);
            Debug.Log("Did Hit");

            WakeUp();

        }

        protected void Follow()
        {
            _agent.destination = _target.position;
            Vector3 worldDeltaPosition = _agent.nextPosition - _agentTransform.position;

            if (worldDeltaPosition.magnitude > _agent.radius)
                _agent.nextPosition = _agentTransform.position;

            if(_distance <= _slowDownDistance)
            {
                _agent.speed = _slowDownSpeed;
            }
            else
            {
                _agent.speed = _fullSpeed;
            }

            if (_agent.destination != null)
            {
                _anim.SetFloat("Velocity", _agent.velocity.magnitude);
            }
            else
            {
                _anim.SetFloat("Velocity", 0f);
            }

            if (_lookAt != null)
            {
                _lookAt.lookAtTargetPosition = _agent.steeringTarget + _lookAt.transform.forward;
            }

        }

        protected void Attack()
        {
            if(_attacking)
            {
                return;
            }

            if(_distance > _attackDistance)
            {
                return;
            }


            _anim.SetTrigger(ATACK_TRIGGER);
            _anim.SetInteger(ATTACK_TYPE, _random.Next(3));
            StartCoroutine(AttackDelay());

        }

        #endregion

        #region Public Methods
        public void WakeUp()
        {
            if (_alive && _puppet.state == PuppetMaster.State.Frozen)
            {
                _puppet.state = PuppetMaster.State.Alive;
            }

            StartCoroutine(WakeUpDelay());
        }
        #endregion

        #region Coroutines
        protected IEnumerator WakeUpDelay()
        {
            yield return new WaitForSeconds(_wakeUpDelay);
            _follow = true;
        }

        protected IEnumerator AttackDelay()
        {
            _attacking = true;

            yield return new WaitForSeconds(_attackSpeed);

            _attacking = false;

        }
        #endregion
        #region Character Controller Events
        /// <summary>
        /// The object has died.
        /// </summary>
        /// <param name="position">The position of the force.</param>
        /// <param name="force">The amount of force which killed the object.</param>
        /// <param name="attacker">The GameObject that killed the object.</param>
        private void OnDeath(Vector3 position, Vector3 force, GameObject attacker)
        {
            Debug.Log("The object was killed by " + attacker);
            _alive = false;
            _puppet.state = PuppetMaster.State.Frozen;

            //freeze position
            _rigidbody.constraints = RigidbodyConstraints.FreezePositionZ | 
                                     RigidbodyConstraints.FreezePositionX |
                                     RigidbodyConstraints.FreezeRotation;

        }
        #endregion
    }
}
