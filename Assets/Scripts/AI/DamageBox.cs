﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Project.Corridor.AI
{
    public class DoDamageEvent : UnityEvent<Collider>
    {
    }
    public class DamageBox : MonoBehaviour
    {
        protected DoDamageEvent _damageEvent;
        public DoDamageEvent DamageEvent { get { return _damageEvent; } }

        private void Awake()
        {
            _damageEvent = new DoDamageEvent();
        }
        private void OnTriggerEnter(Collider other)
        {
            _damageEvent.Invoke(other);
        }

    }
}
