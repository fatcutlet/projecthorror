﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Events;
using Opsive.UltimateCharacterController.Traits;
using RootMotion.Dynamics;

public class Enemy : MonoBehaviour {

    [SerializeField] private GameObject _deathVFX;
    [SerializeField] private string _deathTrigger;
    [SerializeField] private bool _doRagdoll;
    [SerializeField] private float _ragdollDelay;
    [SerializeField] private GameObject[] _disableOnDeath;
    private Animator _animator;
    private Health _health;
    private AttributeManager _attributeManager;
    private PuppetMaster _puppet;
    

    private void Awake()
    {
        EventHandler.RegisterEvent<Vector3, Vector3, GameObject>(gameObject, "OnDeath", OnDeath);
    }

    // Use this for initialization
    void Start () {
        _animator = GetComponent<Animator>();
        _health = GetComponent<Health>();
        if(_health == null)
        {
            _health = GetComponentInChildren<Health>();
        }
        _attributeManager = GetComponent<AttributeManager>();
        _puppet = GetComponentInChildren<PuppetMaster>();

    }
    
    // Update is called once per frame
    void Update () {
        
    }

    public void OnDestroy()
    {
        // Unregister from the event when the component is no longer interested in it. In this example the component is interested for the lifetime of 
        // the component (Awake -> OnDestroy). 
        EventHandler.UnregisterEvent<Vector3, Vector3, GameObject>(gameObject, "OnDeath", OnDeath);
    }

    private void OnDeath(Vector3 position, Vector3 force, GameObject attacker)
    {
        Debug.Log("The object died");
        if(_deathVFX != null)
        {
            Instantiate(_deathVFX, gameObject.transform.position, Quaternion.identity);
        }
        

        if(_animator != null)
        {
            _animator.SetTrigger(_deathTrigger);
        }

        if(_puppet != null)
        {
            _puppet.state = PuppetMaster.State.Dead;
        }

        //disable colliders
        var colliders = GetComponents<Collider>();
        foreach(var collider in colliders)
        {
            collider.enabled = false;
        }

        if (_doRagdoll)
        {
            StartCoroutine(DoRagdoll());
        }

        
    }

    private IEnumerator DoRagdoll()
    {
        yield return new WaitForSeconds(_ragdollDelay);
        
        foreach(var obj in _disableOnDeath)
        {
            obj.SetActive(false);
        }

        _animator.enabled = false;
        _health.enabled = false;
        _attributeManager.enabled = false;

        yield return null;
    }
}
