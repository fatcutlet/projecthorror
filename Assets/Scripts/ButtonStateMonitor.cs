﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Input.VirtualControls;

public class ButtonStateMonitor : MonoBehaviour { 

    private VirtualButton _button;

    // Use this for initialization
    void Start () {
        _button = GetComponent<VirtualButton>();
    }
    
    // Update is called once per frame
    void Update () {
        //Debug.Log("Button pressed: " + _button.IsPressed);
    }
}
