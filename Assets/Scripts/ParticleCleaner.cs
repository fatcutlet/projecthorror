﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCleaner : MonoBehaviour {

    void Start () {
        var ps = GetComponent<ParticleSystem>();
        Destroy(this.gameObject, ps.main.duration);
    }
}
