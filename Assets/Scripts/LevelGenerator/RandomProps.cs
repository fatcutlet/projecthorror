﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Corridor.LevelGenerator
{
    public class RandomProps : MonoBehaviour
    {
        [SerializeField] protected int _coefficient = 2;
        #region unity messages 
        #endregion

        public void RandomizeProps()
        {
            var rnd = new System.Random();
            var props = Enumerable.Range(0, transform.childCount).Select(idx => transform.GetChild(idx).gameObject);
            foreach(var obj in props)
            {
                obj.SetActive(rnd.Next() % _coefficient == 0 ? false : true);
            }


        }

    }

}
