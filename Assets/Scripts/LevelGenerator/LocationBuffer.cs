﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class LocationBuffer : MonoBehaviour
{

    public static readonly string GAME_OBJECT_TAG = "LocBuffer";

    private List<GameObject> _buff;
    private Vector3 _lPosition;

    public ReadOnlyCollection<GameObject> BufferedObjects
    { get { return _buff.AsReadOnly(); } }

    private void Awake()
    {
        _buff = new List<GameObject>();
        _lPosition = transform.position;
    }

    public void Add(GameObject obj)
    {
        if(obj == null || _buff.Contains(obj))
        {
            Debug.LogError("Buffer already contains the game object or obj is null");
            return;
        }

        obj.transform.SetPositionAndRotation(transform.position, Quaternion.identity);
        _buff.Add(obj);
        
        if(_lPosition == transform.position)
        {
            _lPosition += new Vector3(100, 0, 0); 
        }

        transform.SetPositionAndRotation(_lPosition, Quaternion.identity);
            
    }

    public void Remove(GameObject obj)
    {
        if(obj == null || !_buff.Contains(obj))
        {
            Debug.LogError("Buffer does not contain the game object for removing or obj is null");
            return;
        }

        transform.SetPositionAndRotation(obj.transform.position, Quaternion.identity);

        _buff.Remove(obj);
    }
}
