﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Corridor.LevelGenerator
{
    public class Corridor : MonoBehaviour
    {
        [SerializeField] private GenerationNode _leftNode;
        [SerializeField] private GenerationNode _rightNode;

        private RandomProps[] _props;

        public GenerationNode LeftNode
        { get { return _leftNode; } }
        public GenerationNode RightNode
        { get { return _rightNode; } }

        private void Start()
        {
            _props = GetComponentsInChildren<RandomProps>();
        }

        /// <summary>
        /// spawn enemies in the location
        /// </summary>
        public void Spawn()
        {
            var spawners = GetComponentsInChildren<Spawner>();

            if (spawners != null && spawners.Length < 0)
            {
                return;
            }

            foreach (var spawner in spawners)
            {
                //spawner.Spawn();
            }
        }

        public void RandomizeProps()
        {
            if(_props == null || _props.Length == 0)
            {
                return;
            }

            foreach(var p in _props)
            {
                p.RandomizeProps();
            }
        }
    }
}
