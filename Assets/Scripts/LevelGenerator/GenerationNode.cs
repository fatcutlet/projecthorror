﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Project.Corridor.LevelGenerator
{
    public class GenerationNode : MonoBehaviour
    {

        [SerializeField] private GameObject[] _generationObjects;
        [SerializeField] private Vector3[] _offsets;
        [SerializeField] private int _calculateOffsetsFor;
        private GameObject[] _allowedToGenerate;
        public GameObject[] AllowedToGenerate
        {
            set
            {
                _allowedToGenerate = value;
            }
        }
        public LocationBuffer Buffer { get; set; }

        /// <summary>
        /// Generate object at node
        /// </summary>
        /// 
        public GameObject Generate()
        {

            var genObjs = AllowedToGen();

            if(genObjs == null)
            {
                return null;
            }

            var idx = new System.Random().Next(0, genObjs.Length);
            var obj = genObjs[idx];
            var pos = transform.position;

            if(_offsets.Length > idx)
            {
                pos += _offsets[idx];
            }

            return GameObject.Instantiate(obj, pos, Quaternion.identity);
            
        }

        public GameObject Move()
        {
            var genObjs = AllowedToGen();

            if (genObjs == null)
            {
                return null;
            }

            var idx = new System.Random().Next(0, genObjs.Length);

            var obj = genObjs[idx];

            if (obj.scene.name == null)
            {
                Debug.LogError("Can't move object that is not instantinated");
                return null;
            }

            var pos = transform.position;

            if (_offsets.Length == _generationObjects.Length)
            {
                pos += _generationObjects.Select((x, i) => new {x, o = _offsets[i]}).Single(y => y.x == obj).o;
                  
            }

            if (Buffer != null)
                Buffer.Remove(obj);
            obj.transform.SetPositionAndRotation(pos, Quaternion.identity);

            return obj;
        }

        private GameObject[] AllowedToGen()
        {

            if (_allowedToGenerate == null)
            {
                return _generationObjects;
            }

            if (_allowedToGenerate.Length == 0)
            {
                Debug.Log("No allowed objects for generation");
                return null;
            }

            return _generationObjects.Where(x => _allowedToGenerate.Contains(x)).ToArray();
        }

        /// <summary>
        /// It is used in the Editor for offsets calculating 
        /// </summary>
        public void CalculateOffsets()
        {
            if(_calculateOffsetsFor >= _generationObjects.Length)
            {
                Debug.LogError("Too big calculate offsets for");
                return;
            }
            
           if(_offsets.Length < _generationObjects.Length)
            {
                _offsets =_offsets.Concat(new Vector3[_generationObjects.Length - _offsets.Length]).ToArray();
            }

            _offsets[_calculateOffsetsFor] = _generationObjects[_calculateOffsetsFor].transform.position - transform.position;
            
        }
    }
}