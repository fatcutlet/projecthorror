﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace Project.Corridor.LevelGenerator
{
    [System.Serializable]
    public class DetectionEvent : UnityEvent<GameObject>
    {
    }
    public class PlayerDetector : MonoBehaviour {

        private GameObject _parent;
        private DetectionEvent _devent;
        private GameObject _player;

        public DetectionEvent PlayerDetectedEvent
        { get { return _devent; } }

        private void Awake()
        {
            _devent = new DetectionEvent();
        }

        private void Start()
        {
            _parent = gameObject.transform.parent.gameObject;
            if(_parent == null)
            {
                Debug.LogError("PlayerDetector need a parent!");
            }

            _player = GameObject.FindGameObjectWithTag("Player");

        }



        private void OnTriggerEnter(Collider other)
        {
            if(other.transform.IsChildOf(_player.transform))
            {
                Debug.Log("obj is player");
                _devent.Invoke(_parent);
            }
        }

        private void NeedGeneration()
        {
            //foreach (var node in _generationNodes)
            //{
            //    node.Generate();
            //}
        }

    }
}