﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Corridor.LevelGenerator {
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private GameObject[] _objectsToSpawn;
        [SerializeField] private Transform _parent;

        public GameObject Spawn()
        {
            if (_objectsToSpawn == null || _objectsToSpawn.Length == 0)
            {
                return null;
            }

            var toSpawn = _objectsToSpawn[new System.Random().Next(_objectsToSpawn.Length)];

            return Instantiate(toSpawn, transform.position, Quaternion.identity, _parent);
        }
    }
}
