﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelGenerator
{
    public class GenerationObject //TODO: Remove, it is useless
    {
        [SerializeField] private GameObject _gameObject;
        [SerializeField] private float _offsetX;
        [SerializeField] private float _offsetY;
        [SerializeField] private float _offsetZ;
    }
}
