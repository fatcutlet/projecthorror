﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Project.Corridor.LevelGenerator
{
    [CustomEditor(typeof(GenerationNode))]
    public class GenerationNodeEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            var nodeScript = (GenerationNode)target;
            if(GUILayout.Button("Calculate offset"))
            {
                nodeScript.CalculateOffsets();
            }
        }
    }
}
