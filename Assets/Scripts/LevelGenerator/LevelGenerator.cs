﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace Project.Corridor.LevelGenerator
{
    public class LevelGenerator : MonoBehaviour
    {
        [SerializeField] private LinkedList<GameObject> _world;
        //[SerializeField] private GameObject[] _world;
        [SerializeField] private int _worldSize;
        [SerializeField] private GameObject _root;
        [SerializeField] private GameObject _startLocation;
        [Tooltip("Move existing objects instead instantinate new")]
        [SerializeField] private bool _moveInstead;
        [SerializeField] private NavMeshSurface _navSurface;
        [SerializeField] private bool _doNotRemoveRoot = false;
        [SerializeField] private bool _randomProps = true;
        [SerializeField] private List<GameObject> _locationsInBuffer;
        private GameObject _currentLocation;
        private LocationBuffer _buff;

        #region Unity Events
        // Use this for initialization
        void Start()
        {
            InitBuffer();
            InitWorld();
            RegisterAllDetectors();
            GenerateWorld(3);
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Location Buffer
        private void InitBuffer()
        {

            var bgo = GameObject.FindGameObjectWithTag(LocationBuffer.GAME_OBJECT_TAG);
            if(bgo == null)
            {
                return;
            }

            _buff = bgo.GetComponent<LocationBuffer>();

            if(_buff == null)
            {
                return;
            }

            foreach(var loc in _locationsInBuffer)
            {
                _buff.Add(loc);
            }
        }

        private void AddToBuffer(GameObject obj)
        {
            if (_buff == null)
            {
                return;
            }

            _buff.Add(obj);
        }

        private void RemoveFromBuffer(GameObject obj)
        {
            if (_buff == null)
            {
                return;
            }
            _buff.Remove(obj);

        }

        private GameObject[] GetObjectsInBuffer()
        {
            if (_buff == null)
            {
                return null;

            }

            return _buff.BufferedObjects.ToArray();
        }

        private bool InBuffer(GameObject obj)
        {
            if (_buff == null)
            {
                return false;

            }

            return _buff.BufferedObjects.Contains(obj);
        }
            #endregion

            private void InitWorld()
        {
            _world = new LinkedList<GameObject>();
            _world.AddFirst(_root);
        }


        private void GenerateWorld(int size)
        {
            while(_world.Count < size)
            {
                AddLeftLocation();
                AddRightLocation();
            }

            NavRebuild();
        }

        private void AddLeftLocation()
        {
            var location = GenerateAtLeft(_world.First.Value);
            _world.AddFirst(location);
            RegistrDetector(location);
        }

        private void AddRightLocation()
        {
            var location = GenerateAtRight(_world.Last.Value);
            _world.AddLast(location);
            RegistrDetector(location);
        }

        private void RemoveLeftLocation()
        {
            var location = _world.First.Value;
            RemoveOrPutInBuffer(location);
            _world.RemoveFirst();
        }

        private void RemoveRightLocation()
        {
            var location = _world.Last.Value;
            RemoveOrPutInBuffer(location);
            _world.RemoveLast();

        }

        private void RemoveOrPutInBuffer(GameObject location)
        {
            if (!_moveInstead || (!_doNotRemoveRoot && location == _root))
            {
                GameObject.Destroy(location);
            }
            else
            {
                AddToBuffer(location);
            }
        }

        private GameObject GenerateAtLeft(GameObject loc)
        {
            return Generate(loc, 0);   
        }

        private GameObject GenerateAtRight(GameObject loc)
        {
            return Generate(loc, 1);
        }

        private GameObject Generate(GameObject loc, int mode)
        {
            if (mode > 1)
            {
                Debug.Log("This generation mode does not exist");
                return null;
            }

            var corridor = loc.GetComponent<Corridor>();
            if (corridor == null)
            {
                Debug.LogError("Location " + loc.name + " does not have Corridor component.");
                return null;
            }

            var node = mode == 0 ? corridor.LeftNode : corridor.RightNode;

            node.Buffer = _buff;
            node.AllowedToGenerate = GetObjectsInBuffer();

            GameObject newLoc;

            if (_moveInstead)
            {
                newLoc = node.Move();
            }else
            {
                newLoc = node.Generate();
            }

            var corridor2 = newLoc.GetComponent<Corridor>();

            if (corridor2 == null)
            {
                Debug.LogError("Location " + newLoc.name + " does not have Corridor component.");
                return null;
            }

            if (_randomProps)
            {
                corridor2.RandomizeProps();
            }

            corridor2.Spawn();

            return newLoc;

        }
        /// <summary>
        /// Handle event - Player was detected at game object
        /// </summary>
        /// <param name="place">game object with player detected at</param>
        private void PlayerDetected(GameObject place)
        {
            Debug.Log("Player was detected at " + place.name);
            if(_startLocation != null && _startLocation.activeSelf)
            {
                Debug.Log("Disable start location");
                _startLocation.SetActive(false);
                GenerateWorld(_worldSize);
            }

            if(_currentLocation == null)
            {
                _currentLocation = place;
            }
            else if(_currentLocation != place)
            {
                _currentLocation = place;
                UpdateWorld();
            }

        }

        private void UpdateWorld()
        {
            try
            {
                var ci = _world.Select((item, inx) => new { item, inx })
                               .First(x => x.item == _currentLocation).inx;

                var mid = _world.Count / 2;

                if (ci < mid)
                {
                    while(ci != mid)
                    {
                        ci++;
                        AddLeftLocation();
                        RemoveRightLocation();
                    }
                }

                if(ci > mid)
                {
                    while(ci != mid)
                    {
                        ci--;
                        AddRightLocation();
                        RemoveLeftLocation();
                    }
                }

                NavRebuild();

            }
            catch(InvalidOperationException ioe)
            {
                Debug.LogError("Current position is not in the world! \n" + ioe.Message);
                return;
            }
        }

        private void RegisterAllDetectors()
        {
            foreach (var place in  _world)
            {
                RegistrDetector(place);
            }
        }

        /// <summary>
        /// Register event listner for detectors
        /// </summary>
        /// <param name="location">location with listner</param>
        private void RegistrDetector(GameObject location)
        {
            var devent = location.GetComponentInChildren<PlayerDetector>().PlayerDetectedEvent;
            if (devent != null)
            {
                devent.AddListener(PlayerDetected);
            }
            else
            {
                Debug.LogError("Can't fing player detector in " + location.name);
            }
        }

        /// <summary>
        /// Rebuild navigation for all locations
        /// </summary>
        protected void NavRebuild()
        {
            if(_navSurface == null)
            {
                Debug.LogError("NavMeshSurface is missing, can't build navigation");
                return;
            }
            _navSurface.BuildNavMesh();
        }
    }
}
