﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventsLogger : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("EventLogger: OnDrag called");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("EventLogger: OnPointerDown called");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("EventLogger: OnPointerUp called");
    }

}
