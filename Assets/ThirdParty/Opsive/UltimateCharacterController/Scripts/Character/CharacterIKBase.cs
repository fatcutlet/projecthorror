﻿/// ---------------------------------------------
/// Ultimate Character Controller
/// Copyright (c) Opsive. All Rights Reserved.
/// https://www.opsive.com
/// ---------------------------------------------

using UnityEngine;
using Opsive.UltimateCharacterController.StateSystem;
using System;

namespace Opsive.UltimateCharacterController.Character
{
    /// <summary>
    /// Base class for applying IK on the character. Allows other IK solutions to easily be used instead of Unity's IK system.
    /// </summary>
    public abstract class CharacterIKBase : StateBehavior
    {
        [Tooltip("An array of bones that should be smoothed by the Deterministic Object Manager.")]
        [SerializeField] protected Transform[] m_SmoothedBones;

        public Transform[] SmoothedBones { get { return m_SmoothedBones; } }

        // Specifies the limb affected by IK.
        public enum IKGoal
        {
            LeftHand,   // The character's left hand.
            LeftElbow,  // The character's left elbow.
            RightHand,  // The character's right hand.
            RightElbow, // The character's right elbow.
            LeftFoot,   // The character's left foot.
            LeftKnee,   // The character's left knee.
            RightFoot,  // The character's right foot.
            RightKnee,  // The character's right knee.
            Last        // The last entry in the enum - used to detect the number of values.
        }

        // Other objects can modify the final ik position/rotation before it is sent to the IK implementation.
        protected Func<IKGoal, Vector3, Quaternion, Vector3> m_OnUpdateIKPosition;
        protected Func<IKGoal, Quaternion, Vector3, Quaternion> m_OnUpdateIKRotation;
        public Func<IKGoal, Vector3, Quaternion, Vector3> OnUpdateIKPosition { get { return m_OnUpdateIKPosition; } set { m_OnUpdateIKPosition = value; } }
        public Func<IKGoal, Quaternion, Vector3, Quaternion> OnUpdateIKRotation { get { return m_OnUpdateIKRotation; } set { m_OnUpdateIKRotation = value; } }

        /// <summary>
        /// Updates the IK component after the animator has updated during FixedUpdate.
        /// </summary>
        public abstract void FixedMove();

        /// <summary>
        /// Specifies the location of the left or right hand IK target and IK hint target.
        /// </summary>
        /// <param name="itemTransform">The transform of the item.</param>
        /// <param name="itemHand">The hand that the item is parented to.</param>
        /// <param name="nonDominantHandTarget">The target of the left or right hand. Can be null.</param>
        /// <param name="nonDominantHandElbowTarget">The target of the left or right elbow. Can be null.</param>
        public abstract void SetItemIKTargets(Transform itemTransform, Transform itemHand, Transform nonDominantHandTarget, Transform nonDominantHandElbowTarget);

        /// <summary>
        /// Specifies the target location of the limb.
        /// </summary>
        /// <param name="target">The target location of the limb.</param>
        /// <param name="ikGoal">The limb affected by the target location.</param>
        /// <param name="duration">The amount of time it takes to reach the goal.</param>
        public abstract void SetAbilityIKTarget(Transform target, IKGoal ikGoal, float duration);

        /// <summary>
        /// Resets the variables to the default values.
        /// </summary>
        private void Reset()
        {
            var animator = gameObject.GetComponent<Animator>();
            if (animator == null || !animator.isHuman) {
                return;
            }

            // The smoothed bone variable should be populated with all of the humanoid bones.
            var bones = new System.Collections.Generic.List<Transform>();
            AddBone(bones, animator, HumanBodyBones.Hips);
            AddBone(bones, animator, HumanBodyBones.Spine);
            AddBone(bones, animator, HumanBodyBones.Chest);
            AddBone(bones, animator, HumanBodyBones.UpperChest);
            AddBone(bones, animator, HumanBodyBones.Head);
            AddBone(bones, animator, HumanBodyBones.LeftUpperLeg);
            AddBone(bones, animator, HumanBodyBones.LeftLowerLeg);
            AddBone(bones, animator, HumanBodyBones.LeftFoot);
            AddBone(bones, animator, HumanBodyBones.RightUpperLeg);
            AddBone(bones, animator, HumanBodyBones.RightLowerLeg);
            AddBone(bones, animator, HumanBodyBones.RightFoot);
            AddBone(bones, animator, HumanBodyBones.LeftUpperArm);
            AddBone(bones, animator, HumanBodyBones.LeftLowerArm);
            AddBone(bones, animator, HumanBodyBones.LeftHand);
            AddBone(bones, animator, HumanBodyBones.RightUpperArm);
            AddBone(bones, animator, HumanBodyBones.RightLowerArm);
            AddBone(bones, animator, HumanBodyBones.RightHand);

            if (bones.Count > 0) {
                m_SmoothedBones = bones.ToArray();
            }
        }

        /// <summary>
        /// Adds the bone to the list if the bone exists.
        /// </summary>
        /// <param name="bones">The list of current bones.</param>
        /// <param name="animator">A reference to the character's animator.</param>
        /// <param name="bone">The humanoid bone that should be added if it exists.</param>
        private void AddBone(System.Collections.Generic.List<Transform> bones, Animator animator, HumanBodyBones bone)
        {
            var boneTransform = animator.GetBoneTransform(bone);
            if (boneTransform != null) {
                bones.Add(boneTransform);
            }
        }
    }
}